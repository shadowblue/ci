# SPDX-FileCopyrightText: Copyright © 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

workflow:
  rules:
    - if: "$CI_COMMIT_TAG"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: "$CI_COMMIT_BRANCH"

variables:
  LATEST_VERSION: 41
  COSIGN_PASSWORD: ""
  DEFAULT_RECIPE: ./recipes/template.yml
  BASE_IMAGE: base
  # Setup a secure connection with docker-in-docker service
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: /certs
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: $DOCKER_TLS_CERTDIR/client

default:
  image:
    name: ghcr.io/blue-build/cli:latest
    entrypoint: [""]
  services:
    - name: docker:dind
      command: ["--mtu=1300"]

stages:
  - build
  - sign

.matrix:
  parallel:
    matrix:
      - VARIANT: [replaceme]
        FVERSION: [41]

.snippets:
  prepare_template:
    - if [[ -f "./recipes/overrides/${VARIANT}-${FVERSION}.yml" ]]; then
        TEMPLATE_FILE="./recipes/overrides/${VARIANT}-${FVERSION}.yml";
      elif [[ -f "./recipes/overrides/${VARIANT}.yml" ]]; then
        TEMPLATE_FILE="./recipes/overrides/${VARIANT}.yml";
      elif [[ -f "./recipes/overrides/${FVERSION}.yml" ]]; then
        TEMPLATE_FILE="./recipes/overrides/${FVERSION}.yml";
      else
        TEMPLATE_FILE="$DEFAULT_RECIPE";
      fi
    - if [[ -z "${CD_REGISTRY+x}" ]] || [[ -z "${CD_USERNAME+x}" ]] || [[ -z "${CD_PASSWORD+x}" ]]; then
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY";
      else
        docker login -u "$CD_USERNAME" -p "$CD_PASSWORD" "$CD_REGISTRY";
      fi
    - sed -i "s/IMAGE_TAG/${FVERSION}/g" "$TEMPLATE_FILE"
    - |
      if [[ "$VARIANT" == *lts* ]]; then
        sed -i "s/AKMODS_VARIANT/akmods-lts/g" "$TEMPLATE_FILE";
      else
        sed -i "s/AKMODS_VARIANT/akmods/g" "$TEMPLATE_FILE";
      fi
  set_expiration:
    - dnf install -y yq
    - |
      if [[ "$FVERSION" -eq "$LATEST_VERSION" ]]; then
        yq eval '.modules += [{"type": "containerfile", "snippets": ["LABEL quay.expires-after=365d"] }]' -i "$TEMPLATE_FILE";
      elif [[ "$FVERSION" -le "$((LATEST_VERSION - 1))" ]]; then
        yq eval '.modules += [{"type": "containerfile", "snippets": ["LABEL quay.expires-after=180d"] }]' -i "$TEMPLATE_FILE";
      else
        yq eval '.modules += [{"type": "containerfile", "snippets": ["LABEL quay.expires-after=30d"] }]' -i "$TEMPLATE_FILE";
      fi
  build:
    - sed -i "s|IMAGE_REF|${BASE_IMAGE}|" "$TEMPLATE_FILE" # fallback
    - if [[ -z "${CD_REGISTRY+x}" ]] || [[ -z "${CD_NAMESPACE+x}" ]]; then
        bluebuild template "$TEMPLATE_FILE" > Dockerfile;
        IMAGE_PATH="${CI_REGISTRY_IMAGE}/${VARIANT}";
      else
        bluebuild template --registry "$CD_REGISTRY" --registry-namespace "$CD_NAMESPACE" "$TEMPLATE_FILE" > Dockerfile;
        IMAGE_PATH="${CD_REGISTRY}/${CD_NAMESPACE}/${VARIANT}";
      fi
    - docker buildx build --output "type=image,name=${VARIANT},push=true,compression=zstd,oci-mediatypes=true" -t "${IMAGE_PATH}:${FVERSION}" .
    - if [[ "$FVERSION" -eq "$LATEST_VERSION" ]]; then
        docker tag "${IMAGE_PATH}:${FVERSION}" "${IMAGE_PATH}:latest";
        docker push "${IMAGE_PATH}:latest";
      fi
    - if [[ "$FVERSION" -eq "$((LATEST_VERSION - 1))" ]]; then
        docker tag "${IMAGE_PATH}:${FVERSION}" "${IMAGE_PATH}:oldlatest";
        docker push "${IMAGE_PATH}:oldlatest";
      fi

build:
  extends: .matrix
  stage: build
  interruptible: true
  before_script:
    - !reference [.snippets, prepare_template]
    - !reference [.snippets, set_expiration]
  script:
    - |
      if [[ "$VARIANT" == *lts* ]]; then
        lts_suffix="-lts"
      else
        lts_suffix=""
      fi
    - |
      if [[ "$VARIANT" == *nvidia ]]; then
        sed -i "s|IMAGE_REF|${BASE_IMAGE}${lts_suffix}-nvidia|" "$TEMPLATE_FILE";
      elif [[ "$VARIANT" =~ .*nvidia-(.*) ]]; then
        version=${BASH_REMATCH[1]};
        sed -i "s|IMAGE_REF|${BASE_IMAGE}${lts_suffix}-nvidia-${version}|" "$TEMPLATE_FILE";
      else
        sed -i "s|IMAGE_REF|${BASE_IMAGE}${lts_suffix}|" "$TEMPLATE_FILE";
      fi
    - !reference [.snippets, build]

sign:
  extends: .matrix
  stage: sign
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $CD_REGISTRY && $CD_USERNAME && $CD_PASSWORD
  when: always
  before_script:
    - skopeo login -u "$CD_USERNAME" -p "$CD_PASSWORD" "$CD_REGISTRY"
    - cosign login -u "$CD_USERNAME" -p "$CD_PASSWORD" "$CD_REGISTRY"
    # Pulls secure files into the build
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
  script:
    - mkdir -p /etc/containers/registries.d
    - |
      cat <<EOF > /etc/containers/registries.d/${CD_REGISTRY}.yaml
      docker:
        ${CD_REGISTRY}:
          use-sigstore-attachments: true
      EOF
    - IMAGE_PATH="${CD_REGISTRY}/${CD_NAMESPACE}/${VARIANT}"
    - IMAGE_DIGEST=$(skopeo inspect "docker://${IMAGE_PATH}:${FVERSION}" --format "{{.Digest}}")
    - IMAGE_TIMESTAMPED_DIGEST=$(skopeo inspect "docker://${IMAGE_PATH}:${FVERSION}-$(date -u +%Y%m%d)" --format "{{.Digest}}" || true)
    - |
      if ! $(cosign verify --key cosign.pub "${IMAGE_PATH}:${FVERSION}" > /dev/null); then
        cosign sign -y --key .secure_files/cosign.private "${IMAGE_PATH}@$(skopeo inspect "docker://${IMAGE_PATH}:${FVERSION}" --format "{{.Digest}}")";
      fi
    - |
      if [[ "$IMAGE_DIGEST" != "$IMAGE_TIMESTAMPED_DIGEST" ]]; then
        skopeo copy --dest-compress-format zstd:chunked --all --format oci "docker://${IMAGE_PATH}:${FVERSION}" "docker://${IMAGE_PATH}:${FVERSION}-$(date -u +%Y%m%d)";
      fi
    - cosign verify --key cosign.pub "${IMAGE_PATH}:${FVERSION}"
    - cosign verify --key cosign.pub "${IMAGE_PATH}:${FVERSION}-$(date -u +%Y%m%d)"
