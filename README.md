# shadowblue CI template &nbsp; [![pipeline status](https://gitlab.com/shadowblue/template/badges/main/pipeline.svg)](https://gitlab.com/shadowblue/template/-/commits/main)

## How to use

See .gitlab-ci.yml in [template](https://gitlab.com/shadowblue/template) or [main](https://gitlab.com/shadowblue/main) repos

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## Credits

CI code adapted from example in [BlueBuild's cli repo](https://github.com/blue-build/cli#gitlab).

## License

```
Copyright 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
Copyright 2024-2025 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
